import WOW from 'wow.js';
import $ from "jquery";

new WOW().init();

const wow = new WOW(
  {
    boxClass:     'wow',      
    animateClass: 'animated', 
    offset:       1000,          
    mobile:       true,       
    live:         true,       
    callback:     function(box) {
      
      
    },
    scrollContainer: null 
  }
);
wow.init();

const firstAnimation = function () {
  $('.second-block__phone-up').each(
      function () {
        if(~['Android', 'iPhone', 'iPod', 'iPad', 'BlackBerry'].indexOf(navigator.platform)) {
          $(this).delay(300).animate({
            opacity: 1,
            right: '-60px',
            top: '-123px'
          }, 800, 'easeInOutQuint');
        } else if ( window.innerWidth <= 600 ){
          $(this).delay(300).animate({
            opacity: 1,
            right: '-60px',
            top: '-123px'
          }, 800, 'easeInOutQuint');
        } else if ( window.innerWidth >= 601 && window.innerWidth <= 800 ) {
          $(this).delay(300).animate({
            opacity: 1,
            top: '-80px',
            right: 0
          }, 800, 'easeInOutQuint');
          console.log('1');
        } else {
          $(this).delay(300).animate({
            opacity: 1,
            top: '-230px',
            right: 0
          }, 800, 'easeInOutQuint');
        }
      }
  );
  $('.second-block__phone-down').each(
    function () {
      if(~['Android', 'iPhone', 'iPod', 'iPad', 'BlackBerry'].indexOf(navigator.platform)) {
        $(this).delay(300).animate({
          opacity: 1,
          right: '30px',
          top: '402px'
        }, 800, 'easeInOutQuint');
      } else if ( window.innerWidth <= 600 ){
        $(this).delay(300).animate({
          opacity: 1,
          right: '30px',
          top: '402px'
        }, 800, 'easeInOutQuint');
      } else if ( window.innerWidth > 601 && window.innerWidth <= 800 ) {
        $(this).delay(300).animate({
          opacity: 1,
          right: '-110px',
          top: '130px'
        }, 800), 'easeInOutQuint';
      } else {
        $(this).delay(300).animate({
          opacity: 1,
          right: '-110px',
          top: '82px'
        }, 800), 'easeInOutQuint';
      }  
    }
  );
};


$(window).scroll(function() {
  if ($(this).scrollTop()>=350) {
    firstAnimation();
  }
});